/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Institute;

import java.util.ArrayList;

/**
 *
 * @author grant
 */
public class Department {
    private String name;
    private ArrayList <Student> students;

    public Department(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
    
     public void addStudents(Student students){
        this.students.add(students);
    }
    public void removeStudents(Student department){
        this.students.remove(students);
    }
    
}
